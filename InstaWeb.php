<?php
class InstaWeb {
    public function curl($url, $useragent = 0, $cookie = 0, $data = 0, $httpheader = array(), $proxy = 0, $userpwd = 0, $is_socks5 = 0)
    {
        $url = $url;
        $ch  = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        if ($useragent)
        	curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
        if ($proxy)
            curl_setopt($ch, CURLOPT_PROXY, $proxy);
        if ($userpwd)
            curl_setopt($ch, CURLOPT_PROXYUSERPWD, $userpwd);
        if ($is_socks5)
            curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
        if ($httpheader)
            curl_setopt($ch, CURLOPT_HTTPHEADER, $httpheader);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        if ($cookie)
            curl_setopt($ch, CURLOPT_COOKIE, $cookie);
        if ($data):
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        endif;
        $response = curl_exec($ch);
        $httpcode = curl_getinfo($ch);
        if (!$httpcode)
            return false;
        else {
            $header = substr($response, 0, curl_getinfo($ch, CURLINFO_HEADER_SIZE));
            $body   = substr($response, curl_getinfo($ch, CURLINFO_HEADER_SIZE));
            curl_close($ch);
            return array(
                $header,
                $body
            );
        }
    }
    
    public function parseCookies($headers)
    {
        preg_match_all('%Set-Cookie: (.*?);%', $headers, $d);
        $cookie = '';
        for ($o = 0; $o < count($d[0]); $o++):
            $cookie .= $d[1][$o] . ";";
        endfor;
        
        return $cookie;
    }
    
    public function parseCsrf($headers)
    {
        preg_match('#set-cookie: csrftoken=([^;]+)#i', $headers, $token);
        
        return $token[1];
    }
    
    public function cekpoint($url, $data, $csrf, $cookies, $ua)
    {
    	$a = curl_init();
        curl_setopt($a, CURLOPT_URL, 'https://www.instagram.com'.$url);
        curl_setopt($a, CURLOPT_USERAGENT, $ua);
    	curl_setopt($a, CURLOPT_SSL_VERIFYPEER, 0);
    	curl_setopt($a, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($a, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($a, CURLOPT_HEADER, 1);
        curl_setopt($a, CURLOPT_COOKIE, $cookies);
        if($data){
        curl_setopt($a, CURLOPT_POST, 1);	
        curl_setopt($a, CURLOPT_POSTFIELDS, $data);
        }
        if($csrf){
        curl_setopt($a, CURLOPT_HTTPHEADER, array(
                'Connection: keep-alive',
                'Proxy-Connection: keep-alive',
                'Accept-Language: en-US,en',
                'x-csrftoken: '.$csrf,
                'x-instagram-ajax: 1',
                'Referer: '.$url,
                'x-requested-with: XMLHttpRequest',
                'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        ));
        }
        $b = curl_exec($a);
        
        return $b;
    }
    
    public function refresh($url, $cookie, $useragent)
    {
        $return                 = array();
        $url                    = $url;
        $ua                     = $useragent;
        $exc                    = $this->curl($url, $useragent, $cookie);
        $cookie                 = $this->parseCookies($exc[0]);
        $token                  = $this->parseCsrf($exc[0]);
        $return['status']       = "ok";
        $return['message']      = "Login sukses..\n";
        $return['cookies']      = $cookie;
        $return['useragent']    = $ua;
        
        return $return;
    }
    
    function getuid($username)
    {
        $url      = "https://www.instagram.com/" . $username;
        $html     = file_get_contents($url);
        $arr      = explode('window._sharedData = ', $html);
        $arr      = explode(';</script>', $arr[1]);
        $obj      = json_decode($arr[0], true);
        $id       = $obj['entry_data']['ProfilePage'][0]['graphql']['user']['id'];
        
        return $id;
    }

    function getmediaid($url)
    {
        $getid   = file_get_contents("https://api.instagram.com/oembed/?url=".$url);
        $json1   = json_decode($getid);
        $mediaid = $json1->media_id;
        if($mediaid){
            return $mediaid;
        } else {
            return false;
        }
    }
    
    public function login_ig($username, $password)
    {
        $return     = array();
        $url        = 'https://instagram.com/';
        $ua         = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36';
        $exc        = $this->curl($url, $ua);
        $cookie     = $this->parseCookies($exc[0]);
        $token      = $this->parseCsrf($exc[0]);
        $login_url  = 'https://www.instagram.com/accounts/login/ajax/';
        $headers    = array(
            'Cookie: '.$cookie,
            'Origin: https://www.instagram.com',
            'Accept-Encoding: gzip, deflate, br',
            'Accept-Language: en-US,en,q=0.9,id,q=0.8',
            'User-Agent: '.$ua,
            'X-Requested-With: XMLHttpRequest',
            'X-Csrftoken: '.$token,
            'X-Ig-App-Id: 936619743392459',
            'X-Instagram-Ajax: 6cb19191eaa3',
            'Content-Type: application/x-www-form-urlencoded',
            'Accept: */*',
            'Referer: https://www.instagram.com/accounts/login/?source=auth_switcher',
            'Authority: www.instagram.com');
        $data       = 'username='.$username.'&password='.$password.'&queryParams=%7B%22source%22%3A%22auth_switcher%22%7D&optIntoOneTap=true';
        $loginx     = $this->curl($login_url, $ua, $cookie, $data, $headers);
        $login      = json_decode($loginx[1], true);
        if($login['authenticated'] == true){
            $cookie = $this->parseCookies($loginx[0]);
            $csrf   = $this->parseCsrf($loginx[0]);
            $return['status']       = "ok";
            $return['message']      = "Login sukses..\n";
            $return['cookies']      = $cookie;
            $return['useragent']    = $ua;
            $return['csrf']         = $csrf;
        } elseif($login['message'] == 'checkpoint_required'){
            echo "[!] Login Gagal\n";
            echo "[~] Challenge Required\n\n";
            $challenge_csrf     = $token;
            $challenge_url      = $login['checkpoint_url'];
            $challenge_ua       = $ua;
            $challenge_cookie   = $cookie;
            echo "[0] SMS\n[1] Email\n";
            echo "[?] Pilih layanan untuk menerima kode : ";
            $pilihan            = trim(fgets(STDIN, 1024));
            $data               = 'choice='.$pilihan;
            $cekpoint           = $this->cekpoint($challenge_url, $data, $challenge_csrf, $challenge_cookie, $challenge_ua);
            if(strpos($cekpoint, 'status": "ok"') !== false){
                system('clear');
                echo "[~] Kode verifikasi telah terkirim\n";
                echo "[?] Masukkan kode : ";
                $kode           = trim(fgets(STDIN, 1024));
                $data           = 'security_code='.$kode;
                $cekpoint       = $this->cekpoint($challenge_url, $data, $challenge_csrf, $challenge_cookie, $challenge_ua);
                if(strpos($cekpoint, 'status": "ok"') !== false){
                    $cookie = $this->parseCookies($cekpoint);
                    $csrf   = $this->parseCsrf($cekpoint);
                    $return['status']       = "ok";
                    $return['message']      = "Login sukses..\n";
                    $return['cookies']      = $cookie;
                    $return['useragent']    = $challenge_ua;
                    $return['csrf']         = $csrf;
                } else {
                    $return['status']       = "fail";
                    $return['message']      = "[!] Kode verifikasi salah\n";
                    echo $cekpoint;
                }
            } else {
                $return['status']       = "fail";
                $return['message']      = "[!] Kode verifikasi gagal terkirim\n";
            }
        } else {
            $return['status']       = "fail";
            $return['message']      = $loginx[1];
        }
        
        return $return;
    }
}
?>