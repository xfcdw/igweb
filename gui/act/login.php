<?php
session_start();
error_reporting(0);
require('../../lib/InstaWeb.php');
$username       = $_POST['username'];
$password       = $_POST['password'];
$instaweb       = new InstaWeb;
if(empty($password)){
    $msg    = 'Mohon mengisi form dengan benar';
    $msg    = '<div class="alert alert-danger alert-dismissible"><b>Gagal!</b> '.$msg.'.<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a></div>';
    $array  = json_encode(['result' => 0, 'content' => $msg]);
} else {
    $return     = array();
    $url        = 'https://instagram.com/';
    $ua         = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36';
    $exc        = $instaweb->curl($url, $ua);
    $cookie     = $instaweb->parseCookies($exc[0]);
    $token      = $instaweb->parseCsrf($exc[0]);
    $login_url  = 'https://www.instagram.com/accounts/login/ajax/';
    $headers    = array(
        'Cookie: '.$cookie,
        'Origin: https://www.instagram.com',
        'Accept-Encoding: gzip, deflate, br',
        'Accept-Language: en-US,en,q=0.9,id,q=0.8',
        'User-Agent: '.$ua,
        'X-Requested-With: XMLHttpRequest',
        'X-Csrftoken: '.$token,
        'X-Ig-App-Id: 936619743392459',
        'X-Instagram-Ajax: 6cb19191eaa3',
        'Content-Type: application/x-www-form-urlencoded',
        'Accept: */*',
        'Referer: https://www.instagram.com/accounts/login/?source=auth_switcher',
        'Authority: www.instagram.com');
    $data       = 'username='.$username.'&password='.$password.'&queryParams=%7B%22source%22%3A%22auth_switcher%22%7D&optIntoOneTap=true';
    $loginx     = $instaweb->curl($login_url, $ua, $cookie, $data, $headers);
    $login      = json_decode($loginx[1], true);
    if($login['authenticated'] == true){
        $msg            = 'Login success...';
        $msg            = '<div class="alert alert-success alert-dismissible"><b>Sukses!</b> '.$msg.'.<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a></div>';
        $array          = json_encode(['result' => 1, 'content' => $msg]);
    } elseif($login['message'] == 'checkpoint_required'){
        $msg        = $loginx[1];
        $cekpoint   = 1;
        $msg        = '<div class="alert alert-danger alert-dismissible"><b>Gagal!</b> '.$msg.'.<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a></div>';
        $array      = json_encode(['result' => 0, 'content' => $msg, 'cekpoint' => $cekpoint]);
        $cookie     = $instaweb->parseCookies($loginx[0]);
        $csrf       = $instaweb->parseCsrf($loginx[0]);
        $_SESSION['c_cookie']       = $cookie;
        $_SESSION['c_ua']           = $ua;
        $_SESSION['c_token']        = $instaweb->parseCsrf($loginx[0]);
        $_SESSION['c_url']          = $login['checkpoint_url'];
        $_SESSION['c_username']     = $username;
        $_SESSION['c_password']     = $password;
    } else {
        $msg        = $loginx[1];
        $msg        = '<div class="alert alert-danger alert-dismissible"><b>Gagal!</b> '.$msg.'.<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a></div>';
        $array      = json_encode(['result' => 0, 'content' => $msg]);
    }
}
echo $array;
?>