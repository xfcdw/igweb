<?php
session_start();
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="https://getbootstrap.com/favicon.ico">
    <title>Veons Panel</title>
    <!-- Bootstrap core CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.0.0/pulse/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="https://getbootstrap.com/docs/4.0/examples/starter-template/starter-template.css" rel="stylesheet">
    <!-- Fontawesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Datatable CSS -->
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <style>
    body {
    background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAaCAYAAACpSkzOAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABZ0RVh0Q3JlYXRpb24gVGltZQAxMC8yOS8xMiKqq3kAAAAcdEVYdFNvZnR3YXJlAEFkb2JlIEZpcmV3b3JrcyBDUzVxteM2AAABHklEQVRIib2Vyw6EIAxFW5idr///Qx9sfG3pLEyJ3tAwi5EmBqRo7vHawiEEERHS6x7MTMxMVv6+z3tPMUYSkfTM/R0fEaG2bbMv+Gc4nZzn+dN4HAcREa3r+hi3bcuu68jLskhVIlW073tWaYlQ9+F9IpqmSfq+fwskhdO/AwmUTJXrOuaRQNeRkOd5lq7rXmS5InmERKoER/QMvUAPlZDHcZRhGN4CSeGY+aHMqgcks5RrHv/eeh455x5KrMq2yHQdibDO6ncG/KZWL7M8xDyS1/MIO0NJqdULLS81X6/X6aR0nqBSJcPeZnlZrzN477NKURn2Nus8sjzmEII0TfMiyxUuxphVWjpJkbx0btUnshRihVv70Bv8ItXq6Asoi/ZiCbU6YgAAAABJRU5ErkJggg==);
    }
    .my-card
    {
        position:absolute;
        left:40%;
        top:-20px;
        border-radius:50%;
    }
    </style>
  </head>
  <body>
  
    <div class="container">
		<div class="row">
	        <div style="margin-top:50px;" class="col-md-6 offset-md-3 col-sm-8 offset-sm-2">
                <div class="card border-primary">
                    <div class="card-header bg-primary text-white"><i class="fa fa-paper-plane-o"></i> Login Box</div>
                    <div class="card-body">
                        <div id="salsakp8"></div>
                        <form role="form" method="POST" action="act/login.php" autocomplete="off" id="Login-Form">
                            <div class="form-group">
                                <label>Username</label>
                                <input type="text" class="form-control" id="username" name="username" placeholder="Username">
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input type="text" class="form-control" id="password" name="password" placeholder="Password">
                            </div>
                            <button type="submit" id="btn-login8" name="submit" class="btn btn-primary"><i class="fa fa-paper-plane-o"></i> Submit</button>
                        </form>
                        
                        <form role="form" method="POST" action="act/checkpoint.php?step=1" autocomplete="off" id="Login-Checkpoint-1-Form">
                            <div class="form-group">
                                <select class="form-control" name="verifikasi" id="verifikasi">
                                    <option value="999">Pilih metode verifikasi</option>
                                    <option value="1">Email</option>
                                    <option value="0">Nomor Hp</option>
                                </select>
                            </div>
                            <button type="submit" id="btn-login8" name="submit" class="btn btn-primary"><i class="fa fa-paper-plane-o"></i> Next Step</button>
                        </form>
                        
                        <form role="form" method="POST" action="act/checkpoint.php?step=2" autocomplete="off" id="Login-Checkpoint-2-Form">
                            <div class="form-group">
                                <input class="form-control" name="security_code" id="security_code" placeholder="Kode Verifikasi" type="text">
                            </div>
                            <button type="submit" id="btn-login8" name="submit" class="btn btn-primary"><i class="fa fa-paper-plane-o"></i> Verifikasi</button>
                        </form>
                        
                    </div>
                </div><br>
            </div>
        </div>
    </div>
      
      
    <!-- Bootstrap core JavaScript  -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://getbootstrap.com/docs/4.0/assets/js/vendor/popper.min.js"></script>
    <script src="https://getbootstrap.com/docs/4.0/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript">
    $('#Login-Form').css('display', 'block');
    $('#Login-Checkpoint-1-Form').css('display', 'none');
    $('#Login-Checkpoint-2-Form').css('display', 'none');
    $(document).ready(function() {
        $("form#Login-Form").submit(function() {
            var pdata = $(this).serialize();
            var purl = $(this).attr('action');
            $.ajax({
                url: purl,
                data: pdata,
                timeout: false,
                type: 'POST',
                dataType: 'JSON',
                success: function(hasil){
                    $("input").removeAttr("disabled", "disabled");
                    $("button").removeAttr("disabled", "disabled");
                    $("#btn-login8").html('Sign In');
                    if(hasil.result) {
                      //window.location.replace(hasil.redirect);
                      $("#salsakp8").html(hasil.content);
                    } else
                      if(hasil.cekpoint == 1){
                              $('#Login-Form').css('display', 'none');
                              $('#Login-Checkpoint-1-Form').css('display', 'block');
                              $('#Login-Checkpoint-2-Form').css('display', 'none');
                      }
                      $("#salsakp8").html(hasil.content);
                  },
                error: function(a, b, c) {
                    $("input").removeAttr("disabled", "disabled");
                    $("button").removeAttr("disabled", "disabled");
                    $("#btn-login8").html('<i class="fa fa-paper-plane-o"></i> Submit');
                    $("#salsakp8").html(c);
                },
                beforeSend: function() {
                    $("input").attr("disabled", "disabled");
                    $("#btn-login8").html('<i class="fa fa-spinner fa-spin" style="font-size:20px"></i> Loading..');
                    $("#salsakp8").html('');
                    $("button").attr("disabled", "disabled");
                }
            });
            return false
        });
        $("form#Login-Checkpoint-1-Form").submit(function() {
            var pdata = $(this).serialize();
            var purl  = $(this).attr('action');
            $.ajax({
                url: purl,
                data: pdata,
                timeout: false,
                type: 'POST',
                dataType: 'JSON',
                success: function(hasil){
                    $("input").removeAttr("disabled", "disabled");
                    $("button").removeAttr("disabled", "disabled");
                    $("#btn-login8").html('Sign In');
                    if(hasil.result == true){
                        //window.location.replace(hasil.redirect);
                        $("#salsakp8").html(hasil.content);
                        $('#Login-Form').css('display', 'none');
                        $('#Login-Checkpoint-1-Form').css('display', 'none');
                        $('#Login-Checkpoint-2-Form').css('display', 'block');
                    } else
                        $("#salsakp8").html(hasil.content);
                },
                error: function(a, b, c) {
                    $("input").removeAttr("disabled", "disabled");
                    $("button").removeAttr("disabled", "disabled");
                    $("#btn-login8").html('<i class="fa fa-paper-plane-o"></i> Submit');
                    $("#salsakp8").html(c);
                },
                beforeSend: function() {
                    $("input").attr("disabled", "disabled");
                    $("#btn-login8").html('<i class="fa fa-spinner fa-spin" style="font-size:20px"></i> Loading..');
                    $("#salsakp8").html('');
                    $("button").attr("disabled", "disabled");
                }
            });
            return false
        });
        $("form#Login-Checkpoint-2-Form").submit(function() {
            var pdata = $(this).serialize();
            var purl  = $(this).attr('action');
            $.ajax({
                url: purl,
                data: pdata,
                timeout: false,
                type: 'POST',
                dataType: 'JSON',
                success: function(hasil){
                    $("input").removeAttr("disabled", "disabled");
                    $("button").removeAttr("disabled", "disabled");
                    $("#btn-login8").html('Next Step');
                    if(hasil.result == true){
                      $("#salsakp8").html(hasil.content);
                    } else
                      $("#salsakp8").html(hasil.content);
                },
                error: function(a, b, c) {
                    $("input").removeAttr("disabled", "disabled");
                    $("button").removeAttr("disabled", "disabled");
                    $("#btn-login8").html('<i class="fa fa-paper-plane-o"></i> Submit');
                    $("#salsakp8").html(c);
                },
                beforeSend: function() {
                    $("input").attr("disabled", "disabled");
                    $("#btn-login8").html('<i class="fa fa-spinner fa-spin" style="font-size:20px"></i> Loading..');
                    $("#salsakp8").html('');
                    $("button").attr("disabled", "disabled");
                }
            });
            return false
        });
    });
    </script>
    </body>
</html>