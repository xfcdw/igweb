<?php
session_start();
require('../InstaWeb.php');
$instaweb = new InstaWeb;
if($_GET['step'] == '1'){
    $challenge_csrf     = $_SESSION['c_token'];
    $challenge_url      = $_SESSION['c_url'];
    $challenge_ua       = $_SESSION['c_ua'];
    $challenge_cookie   = $_SESSION['c_cookie'];
    $challenge_pw       = $_SESSION['c_password'];
    $pilihan            = $_POST['verifikasi'];
    $data               = 'choice='.$pilihan;
    $cekpoint           = $instaweb->cekpoint($challenge_url, $data, $challenge_csrf, $challenge_cookie, $challenge_ua);
    if(strpos($cekpoint, 'status": "ok"') !== false){
        $android['result']  = true;
        $android['content'] = "Kode verifikasi telah terkirim";
        $android['content'] = '<div class="alert alert-success alert-dismissible"><b>Sukses!</b> '.$android['content'].'.<a href="#" class="close" data-dismiss="alert" aria-label="close">x</a></div> '.$cekpoint;
    } else {
        $android['result']  = true;
        $android['content'] = "Kode verifikasi gagal terkirim";
        $android['content'] = '<div class="alert alert-danger alert-dismissible"><b>Info!</b> '.$android['content'].'. <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a></div>';
    }
    print_r(json_encode($android)); 
} elseif($_GET['step'] == '2'){
    $challenge_csrf     = $_SESSION['c_token'];
    $challenge_url      = $_SESSION['c_url'];
    $challenge_ua       = $_SESSION['c_ua'];
    $challenge_cookie   = $_SESSION['c_cookie'];
    $challenge_pw       = $_SESSION['c_password'];
    $challenge_user     = $_SESSION['c_username'];
    $kode               = $_POST['security_code'];
    $data               = 'security_code='.$kode;
    $cekpoint           = $instaweb->cekpoint($challenge_url, $data, $challenge_csrf, $challenge_cookie, $challenge_ua);
    if(strpos($cekpoint, 'status": "ok"') !== false){
        $cookie = $instaweb->parseCookies($cekpoint);
        $csrf   = $instaweb->parseCsrf($cekpoint);
        $android['result']      = true;
        $android['content']     = "Login Sukses";
        $android['content']     = '<div class="alert alert-success alert-dismissible"><b>Sukses!</b> '.$android['content'].'<a href="#" class="close" data-dismiss="alert" aria-label="close">x/a>.</div><pre>'.$challenge_cookie.'</pre><br><pre>'.$challenge_csrf.'</pre><br><pre>'.$challenge_ua.'</pre>';
        $android['user']        = $reqx->user->username;
        $android['id']          = $reqx->user->pk;
    } else {
        $android['result']      = false;
        $android['content']     = "Kode verifikasi salah";
        $android['content']     = '<div class="alert alert-warning alert-dismissible"><b>Peringatan!</b> '.$android['content'].'.<a href="#" class="close" data-dismiss="alert" aria-label="close">x</a></div> '.$cekpoint;
    }
    print_r(json_encode($android));
} else {
    
}
?>